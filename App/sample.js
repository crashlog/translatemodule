export default {
    placeholder: {
        username: 'User Name',
    },
    button: {
        submit: 'SUBMIT',
    },
    popup: {
        pleaseWait: 'Please wait...',
    },
    confirmationPopup: {
        logout: 'Are you sure you want to logout ?',
    },
    label: {
        settings: 'Settings',
    },
    toast: {
        importBypassOnIOS: 'Import functionality bypassed on IOS devices',
    },
    errors: {
        timeout : 'Dispenser disconnected',
    }
}


import React, { Component, useState } from 'react'
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native'
import { PowerTranslator, ProviderTypes, TranslatorConfiguration, TranslatorFactory } from 'react-native-power-translator'

import en from './en.js'    // Items Count: 230
import sample from './sample.js' // Items Count: 7

const GOOGLE_TRANSLATE_API_KEY = 'AIzaSyD3nGUHO9sCgKJHs5aSKzoRIAtKkltm5IA'

const requiredLang = {}

export default TranslateModule = ({  }) => {
    const [progress, setProgress] = useState(0)
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={() => beginTranslateModule(setProgress)} style={styles.addPadding}>
                <Text>Translate Module</Text>
            </TouchableOpacity>
            <Text>Progress {progress}%</Text>
            <TouchableOpacity onPress={() => console.log("Required Language:\n", JSON.stringify(requiredLang))} style={styles.addPadding}>
                <Text>Log Converted Text</Text>
            </TouchableOpacity>
        </View>
    )
}

const beginTranslateModule = async(onProgress = () => null) => {

    TranslatorConfiguration.setConfig(ProviderTypes.Google, GOOGLE_TRANSLATE_API_KEY, 'id', 'en')
    const translator = TranslatorFactory.createTranslator()
    console.log("Translator Initialized:\n", translator)

    const totalItems = 8
    let counter = 0
    const sourceLanguageString = sample

    Object.keys(sourceLanguageString).map(async(keyOuter) => {
        requiredLang[keyOuter] = {}
        Object.keys(sourceLanguageString[keyOuter]).map(async(keyInner) => {
            counter++
            onProgress(counter/totalItems*100)
            requiredLang[keyOuter][keyInner] = await translator.translate(sourceLanguageString[keyOuter][keyInner])
            if(counter == totalItems) console.log("Task Completed")
        })
    })
    console.log("Translated:", counter)
}



const hitGoogleTranslateAPI = async(text) => {

    const TRANSLATE_API = 'https://translation.googleapis.com/language/translate/v2'

    const method = 'POST'
    const headers = {
        'Content-Type':     'application/json',
        'Authorization':    'Bearer (gcloud auth application-default print-access-token)'
    }
    const body = JSON.stringify({
        q:      'The Great Pyramid of Giza (also known as the Pyramid of Khufu or the Pyramid of Cheops) is the oldest and largest of the three pyramids in the Giza pyramid complex.',
        source: 'en',
        target: 'it',
        format: 'text'
    })

    try {
        let translateAPIResponse = await fetch(TRANSLATE_API, { method, headers, body })
        let translateAPIResponseJson = await translateAPIResponse.json()
        return translateAPIResponseJson
    } catch (error) {
        console.error(error)
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    addPadding: {
        padding: 20
    }
})
